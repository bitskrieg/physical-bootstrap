d-i debian-installer/locale                       string en_US

d-i console-setup/ask_detect                      boolean false

d-i keyboard-configuration/xkb-keymap             select us

d-i netcfg/get_hostname                           string unassigned-hostname
d-i netcfg/get_domain                             string unassigned-domain

d-i hw-detect/load_firmware                       boolean true

d-i mirror/country                                string manual
d-i mirror/http/hostname                          string archive.ubuntu.com
d-i mirror/http/directory                         string /ubuntu
d-i mirror/http/proxy                             string

d-i passwd/root-login                             boolean true
d-i passwd/make-user                              boolean false
d-i passwd/root-password-crypted                  password $6$sSXsfvsKhwy$RrINorhH4lNeNdNbi/vHqCAApM8ID9Lhvmzs6OQMO4791igXZIrhWg6Kyi7XPRGhIZOgGUdCx4prarhaV62id0

d-i clock-setup/utc                               boolean true

d-i time/zone                                     string US/Eastern

d-i clock-setup/ntp                               boolean true
d-i clock-setup/ntp-server                        string 0.pool.ntp.org

d-i partman-lvm/device_remove_lvm                 boolean true
d-i partman-md/device_remove_md                   boolean true
d-i partman-lvm/confirm                           boolean true
d-i partman-auto/disk                             string /dev/sda /dev/sdb
d-i partman-auto/method                           string raid
d-i partman-auto-lvm/new_vg_name                  string vg0
d-i partman-auto-lvm/guided_size                  string 100%
d-i partman-auto/expert_recipe string             \
    efi-lvm ::                                    \
    256 10 256 fat32                              \
    \$primary{ }                                  \
    \$lvmignore{ }                                \
    method{ efi }                                 \
    format{ }                                     \
    .                                             \
    65536 30 1280000000 raid                      \
    \$lvmignore{ }                                \
    \$primary{ }                                  \
    method{ raid }                                \
    .                                             \
    65536 50 65536 ext4                           \
    \$defaultignore{ }                            \
    \$lvmok{ }                                    \
    lv_name{ rootfs }                             \
    method{ format }                              \
    format{ }                                     \
    use_filesystem{ }                             \
    filesystem{ ext4 }                            \
    mountpoint{ / }                               \
    label{ Root }                                 \
    .                                             \
    8192 40 8192 swap                             \
    \$defaultignore{ }                            \
    \$lvmok{ }                                    \
    lv_name{ swap }                               \
    method{ swap }                                \
    format{ }                                     \
    .

d-i partman-auto-raid/recipe string               \
    1 2 0 lvm - /dev/sda2#/dev/sdb2               \
    .
d-i partman-md/confirm                            boolean true
d-i partman-partitioning/confirm_write_new_label  boolean true
d-i partman/choose_partition                      select Finish partitioning and write changes to disk
d-i partman/confirm                               boolean true
d-i partman-md/confirm_nooverwrite                boolean true
d-i partman/confirm_nooverwrite                   boolean true
d-i mdadm/boot_degraded                           boolean true

d-i apt-setup/restricted                          boolean false
d-i apt-setup/universe                            boolean true
d-i apt-setup/backports                           boolean false
d-i apt-setup/multiverse                          boolean false

d-i tasksel/first                                 multiselect standard

d-i pkgsel/include                                string openssh-server xfsprogs curl
d-i pkgsel/update-policy                          select none

d-i grub-installer/only_debian                    boolean true
d-i grub-installer/bootdev                        string default

d-i finish-install/reboot_in_progress             note

#d-i preseed/late_command string                                                                        \
#in-target mdadm --create /dev/md1 --level=10 --run --raid-devices=8 /dev/sd[c-j];                      \
#in-target mdadm --detail --scan /dev/md1 >> /etc/mdadm/mdadm.conf;                                     \
#in-target pvcreate /dev/md1;                                                                           \
#in-target vgcreate vg1 /dev/md1;                                                                       \
#in-target lvcreate -l 90%FREE -n kvmfs vg1;                                                            \
#in-target mkfs.xfs /dev/mapper/vg1-kvmfs;                                                              \
#in-target mkdir /kvmfs;                                                                                \
#in-target echo "/dev/mapper/vg1-kvmfs    /kvmfs    xfs    errors=remount-ro    0    0" >> /etc/fstab;  \

d-i preseed/late_command string                                                                         \
in-target curl -L -o /tmp/bootstrap_salt.sh https://bootstrap.saltstack.com;                            \
in-target /bin/sh /tmp/bootstrap_salt.sh -X
